﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class Teacher : Person
    {
        public string Position { get; set; }
        public string Chear { get; set; }
        public string Vnz { get; set; }

        public Teacher(string firstName, string lastName, string dateOfBirth, string pos, string chear, string vnz)
            : base(firstName, lastName, dateOfBirth)
        {
            Position = pos;
            Chear = chear;
            Vnz = vnz;
        }
        public Teacher(string firstName, string lastName, string dateOfBirth, string pos, string chear)
            : base(firstName, lastName, dateOfBirth)
        {
            Position = pos;
            Chear = chear;
            Vnz = "ЖДТУ";
        }

        public Teacher(string firstName, string lastName, string dateOfBirth, string pos)
            : base(firstName, lastName, dateOfBirth)
        {
            Position = pos;
            Chear = "АтаКі";
            Vnz = "ЖДТУ";
        }
        public Teacher()
        {
            FirstName = "Tom";
            LastName = "Johns";
            DateOfBirth = "11.02.1999";
            Position = "Доцент";
            Chear = "АтаКі";
            Vnz = "ЖДТУ";
        }

        public override void ShowInfo()
        {
            Console.Write("Викладач: ");
            base.ShowInfo();
            Console.WriteLine($"працює на посаді {Position}, на кафедрі {Chear}, в {Vnz} \n");
        }
    }
}
