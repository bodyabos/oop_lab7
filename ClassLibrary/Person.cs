﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }

        public Person(string firstName, string lastName, string dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateOfBirth; 
        }

        public virtual void ShowInfo()
        {
            
            Console.WriteLine($"{FirstName} {LastName} {DateOfBirth}");
        }

        public Person()
        {
            FirstName = "Tom";
            LastName = "Johns";
            DateOfBirth = "11.02.1999";

        }
        public Person(string firstName)
        {
            FirstName = firstName;
            LastName = "Johns";
            DateOfBirth = "11.02.1999";

        }

    }


}