﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class Enterant : Person
    {
        public string PointsZNO { get; set; }
        public string PointsAtes { get; set; }
        public string School { get; set; }

        public Enterant(string firstName, string lastName, string dateOfBirth, string zno, string atestat, string school)
            : base(firstName, lastName, dateOfBirth)
        {
            PointsZNO = zno;
            PointsAtes = atestat;
            School = school;
        }
        public Enterant(string firstName, string lastName, string dateOfBirth)
    : base(firstName, lastName, dateOfBirth)
        {
            PointsZNO = "180";
            PointsAtes = "9";
            School = "№1";
        }
        public Enterant(string firstName, string lastName, string dateOfBirth, string zno, string atestat)
    : base(firstName, lastName, dateOfBirth)
        {
            PointsZNO = zno;
            PointsAtes = atestat;
            School = "№1";
        }
        public Enterant()
        {
            FirstName = "Tom";
            LastName = "Johns";
            DateOfBirth = "11.02.1999";
            PointsZNO = "180";
            PointsAtes = "9";
            School = "№1";
        }
        public override void ShowInfo()
        {
            Console.Write("Абітурієнт: ");
            base.ShowInfo();
           
            Console.WriteLine($"кількість балів сертифікатів ЗНО: {PointsZNO},\n " +
                $"кількість балів за документ про освіту: {PointsAtes},\n " +
                $"назва загальноосвітнього навчального закладу: {School} ");
        }
    }
}
