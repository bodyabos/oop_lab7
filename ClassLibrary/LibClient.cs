﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class LibClient : Person
    {
        public string Number { get; set; }
        public string Date { get; set; }
        public string Pay { get; set; }

        public LibClient(string firstName, string lastName, string dateOfBirth, string num, string date, string pay)
            : base(firstName, lastName, dateOfBirth)
        {
            Number = num;
            Date = date;
            Pay = pay;
        }

        public LibClient(string firstName, string lastName, string dateOfBirth, string num, string date)
    : base(firstName, lastName, dateOfBirth)
        {
            Number = num;
            Date = date;
            Pay = "500";
        }

        public LibClient(string firstName, string lastName, string dateOfBirth, string num)
    : base(firstName, lastName, dateOfBirth)
        {
            Number = num;
            Date = "22.02.2018";
            Pay = "500";
        }

        public LibClient()
        {
            FirstName = "Tom";
            LastName = "Johns";
            DateOfBirth = "11.02.1999";
            Number = "229";
            Date = "22.02.2018";
            Pay = "500";
        }

        public override void ShowInfo()
        {
            Console.Write("Користувач бібліотеки: ");
            base.ShowInfo();
            Console.WriteLine($"\n номер читацького квитка: {Number}, " +
                $"дата видачі: {Date}, розмір щомісячного читацького внеску: {Pay} \n");
        }
    }
}
