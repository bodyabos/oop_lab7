﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
   public class Student : Person
    {
        
        public string Course { get; set; }
        public string Group { get; set; }
        public string Faculty { get; set; }
        public string Vnz { get; set; }

        public void SetCourse(string course)
        {
            Course = course;
        }
        public string GetCourse()
        {
            return Course;
        }
        public void SetGroup(string group)
        {
            Group = group;
        }
        public string GetGroup()
        {
            return Group;
        }

        public void SetFaculty(string facul)
        {
            Faculty = facul;
        }
        public string GetFaculty()
        {
            return Faculty;
        }
        public void SetVnz(string vnz)
        {
            Vnz = vnz;
        }
        public string GetVnz()
        {
            return Vnz;
        }


        public Student( Student pers)
        {
            this.FirstName = pers.FirstName;
            this.LastName = pers.LastName;
            this.DateOfBirth = pers.DateOfBirth;
            

        }

        public Student(string firstName, string lastName, string dateOfBirth, string cors, string gro, string facul, string vnz)
            : base(firstName, lastName, dateOfBirth)
        {
            Course = cors;
            Group = gro;
            Faculty = facul;
            Vnz = vnz;
        }
        public Student(string firstName, string lastName, string dateOfBirth, string cors, string gro)
    : base(firstName, lastName, dateOfBirth)
        {
            Course = cors;
            Group = gro;
            Faculty = "Фікт";
            Vnz = "ЖДТУ";
        }
        public Student(string firstName, string lastName, string dateOfBirth)
: base(firstName, lastName, dateOfBirth)
        {
            Course = "1";
            Group = "КБ-1";
            Faculty = "Фікт";
            Vnz = "ЖДТУ";
        }

        public Student()
        {
            FirstName = "Tom";
            LastName = "Johns";
            DateOfBirth = "11.02.1999";
            Course = "1";
            Group = "КБ-1";
            Faculty = "Фікт";
            Vnz = "ЖДТУ";
        }

        public override void ShowInfo()
        {
            Console.Write("Студент: ");
            base.ShowInfo();
            Console.WriteLine($"навчається на {Course} курсі, в групі {Group} ," +
                $" на факультеті {Faculty}, в {Vnz} ");
        }
    }
}
