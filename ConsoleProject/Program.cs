﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleProject
{
    class Program
    {
        public static Person[] person;
        public static int n=0;
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №7";
            Console.SetWindowSize(100, 25);
            Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.DarkBlue; Console.Clear();

            //int n = 10;
            person = new Person[100];

            int x=1;
            
            while (x != 0)
            {
                Console.WriteLine("\t1. Додати людину\n\t" +
                    "2. Додати студента\n\t" +
                    "3. Додати абітурієнта\n\t" +
                    "4. Додати викладача\n\t" +
                    "5. Додати користувача бібліотеки\n\t" +
                    "6. Роздрукувати інформацію про людей\n\t" +
                 
                    "0. Вихід" +
                    "\n-->");

                while (!int.TryParse(Console.ReadLine(), out x))
                {
                    Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");
                }


                switch (x)
                {
                    case 1: { CreatePerson(); break; }
                    case 2: { CreateStudent(); break; }
                    case 3: { CreateEnterant(); break; }
                    case 4: { CreateTeacher(); break; }
                    case 5: { CreateLib(); break; }
                    case 6: { PersonShow();  break; }
                    
                }
            }



            Console.Read();
        }

        public static void CreateLib()
        {
            LibClient librant = new LibClient();
            person[n] = new Person();

            Console.WriteLine("Введіть ім'я:");
            librant.FirstName = Console.ReadLine();
            Console.WriteLine("Введіть прізвище:");
            librant.LastName = Console.ReadLine();
            Console.WriteLine("Введіть дату народження:");
            librant.DateOfBirth = Console.ReadLine();
            Console.WriteLine("Введіть номер читацького квитка:");
            librant.Number = Console.ReadLine();
            Console.WriteLine("Введіть дата видачі:");
            librant.Date = Console.ReadLine();
            Console.WriteLine("Введіть розмір щомісячного читацького внеску:");
            librant.Pay = Console.ReadLine();

            person[n] = librant;
            n++;
        }

        public static void CreateTeacher()
        {
            Teacher teach = new Teacher();
            person[n] = new Person();

            Console.WriteLine("Введіть ім'я:");
            teach.FirstName = Console.ReadLine();
            Console.WriteLine("Введіть прізвище:");
            teach.LastName = Console.ReadLine();
            Console.WriteLine("Введіть дату народження:");
            teach.DateOfBirth = Console.ReadLine();
            Console.WriteLine("Введіть посаду:");
            teach.Position = Console.ReadLine();
            Console.WriteLine("Введіть кафедру:");
            teach.Chear = Console.ReadLine();
            Console.WriteLine("Введіть навчальний заклад:");
            teach.Vnz = Console.ReadLine();

            person[n] = teach;
            n++;
        }

        public static void CreateEnterant()
        {
            Enterant ent = new Enterant();
            person[n] = new Person();


            Console.WriteLine("Введіть ім'я:");
            ent.FirstName = Console.ReadLine();
            Console.WriteLine("Введіть прізвище:");
            ent.LastName = Console.ReadLine();
            Console.WriteLine("Введіть дату народження:");
            ent.DateOfBirth = Console.ReadLine();
            Console.WriteLine("Введіть бал ЗНО:");
            ent.PointsZNO = Console.ReadLine();
            Console.WriteLine("Введіть бал атестату:");
            ent.PointsAtes = Console.ReadLine();
            Console.WriteLine("Введіть навчальний заклад:");
            ent.School = Console.ReadLine();
            
            person[n] = ent;
            n++;
        }

        public static void CreateStudent()
        {
           
            Student stud = new Student();
            person[n] = new Person();

            Console.WriteLine("Введіть ім'я:");
            stud.FirstName = Console.ReadLine();
            Console.WriteLine("Введіть прізвище:");
            stud.LastName = Console.ReadLine();
            Console.WriteLine("Введіть дату народження:");
            stud.DateOfBirth = Console.ReadLine();
            Console.WriteLine("Введіть курс:");
            stud.Course = Console.ReadLine();
            Console.WriteLine("Введіть групу:");
            stud.Group = Console.ReadLine();
            Console.WriteLine("Введіть факультет:");
            stud.Faculty = Console.ReadLine();
            Console.WriteLine("Введіть навчальний заклад:");
            stud.Vnz = Console.ReadLine();
            
            person[n] = stud;

            n++;
        }

        public static void PersonShow()
        {
            if (n != 0)
            {
                for (int i = 0; i < n; i++)
                {
                    person[i].ShowInfo();
                }
            }
            else Console.WriteLine("Ви не додали людей");
        }

        public static void CreatePerson()
        {

               var pers = new Person();

                Console.WriteLine("Введіть ім'я:");
                pers.FirstName = Console.ReadLine();
                Console.WriteLine("Введіть прізвище:");
                pers.LastName = Console.ReadLine();
                Console.WriteLine("Введіть дату народження:");
                pers.DateOfBirth = Console.ReadLine();

            person[n] = pers;
            n++;
        }
    }
}
